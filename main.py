
from zipfile import ZipFile
from urllib.request import urlopen
from io import BytesIO
from bs4 import BeautifulSoup
import urllib.request
import requests
import datetime
import math
from datetime import timedelta, datetime
import os

url = 'https://www.mbd.hu/uris'
sauce = urllib.request.urlopen(url).read()
soup = BeautifulSoup(sauce, 'lxml')

folder = soup.findAll("a")[5]
url_name = 'https://www.mbd.hu/uris/%s' % folder.text + 'urilist.zip'
file = open('testfile.txt', 'a')
if folder.text in open('testfile.txt').read():
    print("már hozzáadtam")
else:
    file.write(folder.text+"\n")

file = open('testfile.txt', 'r')
lines = file.read().split('\n')
psw = lines[-3]
datetime_object = datetime.strptime(psw, '%Y_%m_%d_%H_%M_%S/')
real_time = (datetime_object + timedelta(hours=2)).timestamp()

epochtime = int(math.floor(real_time))
password = str(epochtime)

zipurl = url_name
print(zipurl)

if (os.path.isfile('urilist/urilist.txt')):
    os.remove('urilist/newuri.txt')
    os.rename('urilist/urilist.txt', 'urilist/newuri.txt')

with urlopen(zipurl) as zipresp:
    with ZipFile(BytesIO(zipresp.read())) as zfile:
        zfile.extractall('urilist', pwd=bytes(password, 'utf-8'))

with open('urilist/urilist.txt', 'r') as file1:
    with open('urilist/newuri.txt', 'r') as file2:
        same = set(file1).intersection(file2)

same.discard('\n')

with open('urilist/results.txt', 'w') as file_out:
    for line in same:
        file_out.write(line)

# os.remove('urilist/urilist.txt')
